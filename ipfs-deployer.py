import re
import requests
import json
import logging
from pathlib import Path

from services import ConfigParser, Services

if __name__ == "__main__":
    Config = ConfigParser.Config()
    Services = Services.Services()

    logging.info(Services.CheckDestinationDirectory())

    #Upload media or downloads files to IPFS:
    uploaded_files = []
    for single_format in Config.ipfs_filter:
        document_list = Services.ReadPath(single_format)
        for document in document_list:
            if single_format == document['name'].split('.')[1]:
                #TODO inserire controlli di verifica a livello utente per caricare permanentemente il tutto su IPFS.
                document['CID'] = Services.AddContent(document['name'], document['content'])['Hash']
                uploaded_files.append(document)
    
    #Remap links on tracked files:
    for single_format in Config.ipns_filter:
        document_list = Services.ReadPath(single_format)
        for document in document_list:
            if single_format == document['name'].split('.')[1]:
                if re.search('<img src=(.*)', document['content']) is not None: #TODO mettere tutti i casi possibili (apici, spazi, con o senza slash, etc.)
                    content_path_position = re.search('<img src="(.*)"/>', document['content']).regs[1]
                    content_path = document['content'][content_path_position[0]:content_path_position[1]]
                    for path_with_cid in uploaded_files:
                        if content_path in path_with_cid['path']:
                            print() #TODO TEST
                            new_page = document['content'].replace(content_path, Config.ipfs_prefix + path_with_cid['CID'])

                            f = open(Config.ipfs_formatted_files_directory + "/" + document['name'], "w")   #TODO mettere cartella in configurazione
                            f.write(new_page)
                            print(new_page) #TODO TEST
                            f.close()
                #TODO Mettere casi con link, video, audio, etc.
                #TODO Mettere casi di css, js, etc.
                else:
                    f = open(Config.ipfs_formatted_files_directory + "/" + document['name'], "w")   #TODO mettere cartella in configurazione
                    f.write(document['content'])
                    f.close()
            
    #TODO inserire garbage collection




    #Upload tracked files to IPFS:
