import requests
import json
import logging
from pathlib import Path
from . import ConfigParser

class Services():
    def __init__(self,):
        self.file_list = []
        self.document = {}
        self.Config = ConfigParser.Config()

    def ReadPath(self, format):
        for p in Path(self.Config.ipfs_local_files_directory).glob('**/*.'+format):    #TODO mettere cartella da configurazione
            self.document['name'] = p.name
            if self.isBinary(str(p)) == False:
                self.document['content'] = p.read_text()
            else:
                self.document['content'] = open(str(p),'rb')
            self.document['path'] = str(p)
            self.file_list.append(self.document.copy())
        return self.file_list

    def isBinary(self, filename):
        try:
            with open(filename, 'tr') as check_file:
                check_file.read()
                return False
        except:
            return True

    def AddContent(self, name, content):
        payload = {name: content}
        try:
            response = requests.post(url = self.Config.ipfs_api_url, files=payload)
            ipfs_content = json.loads(response.text)
            ipfs_content_name = ipfs_content['Name']
            ipfs_content_hash = ipfs_content['Hash']
            ipfs_content_size = ipfs_content['Size']    #Useless?
            print("File " + ipfs_content_name + " uploaded successfully on IPFS:")  #TODO comunicare magari che è già presente in caso vi sia già
            print("     " + ipfs_content_hash)
            return ipfs_content
            #TODO loggare file caricati correttamente
        except Exception as Ex:
            logging.error("Impossible to upload file " + name + " to IPFS.")
            logging.error(Ex)
            logging.error("Status code: " + response.status_code)
            #TODO loggare errori

    def CheckDestinationDirectory(self):
        if not os.path.exists(ipfs_formatted_files_directory):
            print("AAAAA")
        else:
            print("BBBBBBBB")