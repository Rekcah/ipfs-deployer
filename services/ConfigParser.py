import configparser

class Config():
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')

        self.ipfs_prefix = config['ipfs-settings']['ipfs_prefix']
        self.ipns_prefix = config['ipfs-settings']['ipns_prefix']
        self.ipfs_api_url = config['ipfs-settings']['ipfs_api_url']
        self.ipns_filter = config['ipfs-settings']['ipns_filter'].split(',')
        self.ipfs_filter = config['ipfs-settings']['ipfs_filter'].split(',')
        self.ipfs_formatted_files_directory = config['ipfs-settings']['ipfs_formatted_files_directory']
        self.ipfs_local_files_directory = config['ipfs-settings']['ipfs_local_files_directory']